set nocompatible

filetype plugin indent on

"
" General
"
set nobackup
set nowritebackup
set noswapfile
set linebreak
"set mouse=a
set scrolloff=10
set nofoldenable
" set laststatus=2
syn match Braces display '[{}()\[\]\'"]'
hi Braces guifg=red
hi MatchParen cterm=none ctermbg=none ctermfg=yellow
set backspace=2 " make backspace work like most other apps
set backspace=indent,eol,start
" set modeline
set ttimeoutlen=0
" set viminfo='10,\"100,:20,%,n~/.viminfo
" Disable comments' autoexpanding
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o " formatoptions+=t
"
" Bindings
"
let mapleader=','
map <F3> :noh<Enter>
map <F4> "*p
map <F5> :set nonu<Enter>
map <F6> :set nu<Enter>
"map <F7> <Esc>gg O# vim: filetype=
map <F7> <Esc>:NERDTreeToggle<Enter>
nnoremap <C-o> <Esc>o<Esc>
inoremap <C-o> <Esc>o<Esc>
map <F9> :AutoSaveToggle<Enter>
"map <C-b> <Esc>:BuffergatorToggle<Enter>
map <Up> gk
map <Down> gj
map <Home> g<Home>
map <End>  g<End>
inoremap <buffer> <silent> <Up>   <C-o>gk
inoremap <buffer> <silent> <Down> <C-o>gj
inoremap <buffer> <silent> <Home> <C-o>g<Home>
inoremap <buffer> <silent> <End>  <C-o>g<End>
map { gT
map } gt
map [ gT
map ] gt
set pastetoggle=<F2>
map ; :
map q :q<Enter>
nnoremap <C-t> "=strftime("%Y-%m-%d %H:%M:%S UTC%z")<CR>P
inoremap <C-t> <C-R>=strftime("%Y-%m-%d %H:%M:%S UTC%z")<CR>
nnoremap <C-d> "=strftime("/%Y-%m-%d %H:%M:%S UTC%z")<CR>P
inoremap <C-d> <C-R>=strftime("/%Y-%m-%d %H:%M:%S UTC%z")<CR>
inoremap <C-k> <Esc>
"
" Coding
"
syntax on
set background=dark
set nu
set nuw=4
"colorscheme pyte
" Indents
set autoindent
set shiftwidth=4
set tabstop=4
set softtabstop=4
set expandtab
set shiftround

set laststatus=2
"закрытие скобок
" imap [ []<LEFT>
" imap ( ()<LEFT>
" imap { {}<LEFT>
" "закрытие кавычек
" inoremap " ""<LEFT>
" inoremap ' ''<LEFT>
"
"set fdm=syntax
"let loaded_matchparen = 0

" Pascal
"let pascal_no_functions=1
"let pascal_delphi=1
let pascal_fpc=1
let pascal_one_line_string=1
let pascal_no_tabs=1
au BufRead,BufNewFile *.pp set filetype=pascal

"
" vim-plug
"
call plug#begin('~/.vim/plugged')
"Plug 'lyokha/vim-xkbswitch'
"let g:XkbSwitchEnabled = 1
"let g:XkbSwitchLib = '/home/user/src/xkb-switch/libxkbswitch.so'
"let g:XkbSwitchIMappings = ['ru']
"let g:XkbSwitchSkipIMappings = {'*' : ['[', ']', '{', '}', "'", '<', '>', ',', '.', '"']}

Plug 'mattn/emmet-vim'
Plug 'Raimondi/delimitMate', { 'for': ['html', 'css'] }
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
"Plug 'Yggdroot/indentLine', { 'for': 'python' }
Plug 'ntpeters/vim-better-whitespace'
Plug 'MattesGroeger/vim-bookmarks'
Plug 'vimoutliner/vimoutliner'
Plug 'scrooloose/nerdtree'
Plug 'maksimr/vim-jsbeautify'
autocmd FileType javascript noremap <buffer>  <c-f> :call JsBeautify()<cr>
" for json
autocmd FileType json noremap <buffer> <c-f> :call JsonBeautify()<cr>
" for jsx
autocmd FileType jsx noremap <buffer> <c-f> :call JsxBeautify()<cr>
" for html
autocmd FileType html noremap <buffer> <c-f> :call HtmlBeautify()<cr>
" for css or scss
autocmd FileType css noremap <buffer> <c-f> :call CSSBeautify()<cr>

Plug 'fatih/vim-go'
Plug 'Chiel92/vim-autoformat'
Plug 'vim-scripts/vim-auto-save'
let g:auto_save_in_insert_mode = 0

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

let g:airline_theme='distinguished'
let g:airline_powerline_fonts = 1

Plug 'bling/vim-bufferline'
let g:bufferline_echo = 0

set noshowmode
let g:indentLine_enabled = 1
let g:indentLine_char = '¦'
let g:indentLine_color_term = 4

Plug '~/.vim-local/cvim'
Plug 'artur-shaik/vim-javacomplete2'
Plug 'lervag/vimtex'
let g:vimtex_fold_enabled = 0

Plug 'jceb/vim-orgmode'
Plug 'tell-k/vim-autopep8'
Plug 'vimoutliner/vimoutliner'
Plug 'scrooloose/nerdtree'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-speeddating'
Plug 'scrooloose/syntastic'
Plug 'othree/html5-syntax.vim'
Plug 'Valloric/MatchTagAlways'
Plug 'othree/html5.vim'
Plug 'hail2u/vim-css3-syntax'
Plug 'mattn/emmet-vim'
Plug 'tpope/vim-surround'
Plug 'Shougo/neocomplete.vim'
Plug 'scrooloose/syntastic'
Plug 'jeetsukumaran/vim-buffergator'
"Plug 'klen/python-mode'
Plug 'mbbill/undotree'

call plug#end()

"
" Templates
"
function! s:format_template()
    set report=999

    let filename = expand('%:t')
    execute '%s/%vim%filename%/' . filename . '/geI'
    let header_var = substitute(toupper(filename), '\.', '_', 'ge')
    execute '%s/%vim%header_var%/' . header_var . '/geI'
    execute '%s/%vim%year%/\=strftime("%Y")/geI'

    set report=2
endfunction

autocmd BufNewFile  *.py     0r ~/.vim/templates/py
autocmd BufNewFile  *.c     0r ~/.vim/templates/c
autocmd BufNewFile  *       call s:format_template()
if has("autocmd")
    au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

"
" Hard settings
"

:noh
"autocmd BufWritePre * :%s/\s\+$//e
"set virtualedit=all

:hi TabLineFill ctermfg=Grey ctermbg=Grey
:hi TabLine ctermfg=Black ctermbg=Grey
:hi TabLineSel ctermfg=Black ctermbg=Green
":hi TabLineFill ctermfg=White ctermbg=White
" :hi TabLine ctermfg=Blue ctermbg=White
" :hi TabLineSel ctermfg=White ctermbg=Blue


" Restore current position
function! ResCur()
    if line("'\"") <= line("$")
        normal! g`"
        return 1
    endif
endfunction

augroup resCur
    autocmd!
    autocmd BufWinEnter * call ResCur()
augroup END

autocmd BufRead,BufNewFile *.otl      setfiletype help
autocmd BufRead,BufNewFile *.BAS,*.BAS-UTF8      setfiletype basic
