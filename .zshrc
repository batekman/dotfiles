for profile in '/etc/profile' "$HOME/.profilerc"; do
    if [ -f "$profile" ]; then
        source "$profile"
    fi
done

# The following lines were added by compinstall
zstyle :compinstall filename '/home/user/.zshrc'

fpath=(~/.zsh/completion $fpath)

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
#HISTFILE=~/.histfile
HISTFILE=~/.bash_history
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd extendedglob notify
bindkey -e
# End of lines configured by zsh-newuser-install

zstyle ':completion:*' menu select
setopt COMPLETE_ALIASES
setopt HIST_IGNORE_DUPS

autoload -Uz chpwd_recent_dirs cdr add-zsh-hook
add-zsh-hook chpwd chpwd_recent_dirs

autoload -U colors
colors

zstyle ':completion:*' rehash true

setopt PROMPT_SUBST

update-title() {
    #wmctrl -r :ACTIVE: -N "Shell — $PWD"
}

if [ $UID -eq 0 ]; then
    export PS1=$'`update-title`%{$fg[red]%}#%{$reset_color%} '
else
    export PS1=$'`update-title`%{$fg[green]%}\$%{$reset_color%} '
fi

export KEYTIMEOUT=1

bindkey -v

bindkey '^[[5~' history-beginning-search-backward
bindkey '^[[6~' history-beginning-search-forward

bindkey '\e[1~'   beginning-of-line  # Linux console
bindkey '\e[H'    beginning-of-line  # xterm
bindkey '\eOH'    beginning-of-line  # gnome-terminal
#bindkey '\e[2~'   overwrite-mode     # Linux console, xterm, gnome-terminal
#bindkey '\e[3~'   delete-char        # Linux console, xterm, gnome-terminal
bindkey '\e[4~'   end-of-line        # Linux console
bindkey '\e[F'    end-of-line        # xterm
bindkey '\eOF'    end-of-line        # gnome-terminal

# Use vim cli mode
bindkey '^P' history-beginning-search-backward
bindkey '^N' history-beginning-search-forward

# backspace and ^h working even after
# returning from command mode
bindkey '^?' backward-delete-char
bindkey '^h' backward-delete-char

# ctrl-w removed word backwards
bindkey '^w' backward-kill-word

# ctrl-r starts searching history backward
bindkey '^r' history-incremental-search-backward

bindkey -M vicmd '^' beginning-of-line
bindkey -M vicmd '$' end-of-line

export RPS1=$'%~'

bindkey '^k' vi-cmd-mode # <C-k> for going to command mode

bindkey -M vicmd ' ' execute-named-cmd # Space for command line mode

# Home key variants
bindkey '\e[1~' vi-beginning-of-line
bindkey '\eOH' vi-beginning-of-line

# End key variants
bindkey '\e[4~' vi-end-of-line
bindkey '\eOF' vi-end-of-line

bindkey -M viins '^o' vi-backward-kill-word

bindkey -M vicmd 'yy' vi-yank-whole-line
bindkey -M vicmd 'Y' vi-yank-eol

bindkey -M vicmd 'y.' vi-yank-whole-line
bindkey -M vicmd 'c.' vi-change-whole-line
bindkey -M vicmd 'd.' kill-whole-line

bindkey -M vicmd 'u' undo
bindkey -M vicmd 'U' redo

bindkey -M vicmd 'H' run-help
bindkey -M viins '\eh' run-help

bindkey -M vicmd 'k' history-beginning-search-backward
bindkey -M vicmd 'j' history-beginning-search-forward

bindkey -M vicmd '\-' vi-repeat-find
bindkey -M vicmd '_' vi-rev-repeat-find

bindkey -M viins '\e.' insert-last-word
bindkey -M vicmd '\e.' insert-last-word

bindkey -M viins '^a' beginning-of-line
bindkey -M viins '^e' end-of-line

su-root() {
    echo >&2
    if [ $UID -eq 0 ]; then
        echo "You are already have UID=0." >&2
        zle reset-prompt
        return
    fi
    sudo -i <$TTY
    zle reset-prompt
}

zle -N su-root

bindkey -M viins '^r' su-root

choose-dir() {
    TMP=`mktemp`
    ranger --choosedir="$TMP" <$TTY
    cd "`head -n1 \"$TMP\"`"
    rm "$TMP"
    #zle reset-prompt
}

#zle -N choose-dir

#bindkey -M viins '^n' choose-dir

go-up() {
    cd ..
    #update-title
    zle reset-prompt
}

zle -N go-up

bindkey -M viins '^u' go-up

function run-again {
    # get previous history item
    zle up-history
    # confirm command
    zle accept-line
}

# define run-again widget from function of the same name
zle -N run-again

# bind widget to Ctrl+X in viins mode
bindkey -M viins '^X' run-again
# bind widget to Ctrl+X in vicmd mode
bindkey -M vicmd '^X' run-again

# VIM-mode status line
function zle-line-init zle-keymap-select {
    RPS1='%~ | '"${${KEYMAP/vicmd/-- NORMAL --}/(main|viins)/-- INSERT --}"
    RPS2=$RPS1
    #update-title
    zle reset-prompt
}

zle -N zle-line-init
zle -N zle-keymap-select

# Case-insensetive completion

_cscomplete() {
  _complete
  return 1
}
#zstyle ':completion:*:cscomplete:*' group-name case-sensitive
zstyle ':completion:*:complete:*' group-name case-insensitive
#zstyle ':completion:*' group-order case-sensitive case-insensitive
#zstyle ':completion:*::::' completer _cscomplete _complete
zstyle ':completion:*::::' completer _complete
zstyle ':completion:*:cscomplete:*:*' matcher-list ''
zstyle ':completion:*:complete:*' matcher-list 'm:{a-z}={A-Z}'


vimrc() {
    vim $HOME/.vimrc
}

bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

unset EQUALS
#export TERM='xterm'

__git_files () {
    _wanted files expl 'local files' _files
}

unsetopt auto_remove_slash
setopt dotglob

zstyle ':completion:*' hosts off
