"syntax match BasicLangParam /^[^=]\+/
"syntax match BasicLangValue /[^=]\+$/
syntax match BasicLangVariable '\<.*\$\>'

syntax keyword BasicLangKeywords PRINT OPEN IF DO WHILE WEND
syntax keyword BasicLangKeywords FOR NEXT
syntax keyword BasicLangKeywords OPEN CLOSE
syntax keyword BasicLangKeywords SELECT CASE IS
syntax keyword BasicLangKeywords END
syntax keyword BasicLangKeywords SCREEN

syntax keyword BasicLangFunctions LEN INKEY INPUT CHR

"highlight BasicLangParam ctermfg=cyan
"highlight BasicLangValue ctermfg=blue
highlight BasicLangVariable cterm=none ctermfg=NONE
highlight BasicLangKeywords cterm=bold ctermfg=brown
highlight BasicLangFunctions cterm=bold ctermfg=cyan
