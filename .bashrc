# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !


# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi


# Put your fun stuff here.
. /etc/profile
alias brightness="sudo ~user/bin/brightness"
alias emerge="sudo python3.5 /usr/bin/emerge"
alias ls="ls --group-directories-first --color=auto"
alias i3config="vim ~/.i3/config"

export TEXMFLOCAL="$HOME/.local/share/texmf"

st() {
   git status --short | egrep -v '^\?\? '
}

#if [ -z $DISPLAY ]; then
#   startx
#fi
alias viworld="sudo /usr/bin/vim /var/lib/portage/world"
alias viuse="sudo /usr/bin/vim /etc/portage/package.use/custom"
alias vimakeconf="sudo /usr/bin/vim /etc/portage/make.conf"
alias iotop="sudo iotop"

tailfetch() {
    sudo tail -f /var/log/emerge-fetch.log
}

vimrc() {
    vim "$HOME/.vimrc"
}

vibashrc() {
    vim "$HOME/.bashrc"
}

choose-dir() {
    TMP=`mktemp`
    ranger --choosedir="$TMP"
    cd "`head -n1 \"$TMP\"`"
    rm "$TMP"
}

#export PS1='\n\[\033[01;32m\]\$\[\033[00m\] '
