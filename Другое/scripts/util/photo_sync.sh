#! /usr/bin/env bash

set -e

DEV="$1"
PHOTODIR="$HOME/photo"
DEST="$HOME/photo/SD_`date '+%Y-%m-%d_%H:%M:%S'`"

if [ -z "$DEV" ]; then
    exit 2
fi

Mount() {
    if ! egrep --quiet "^$1\s+" /etc/mtab ; then
        udisksctl mount --block-device "$1" > /dev/null
    fi
    egrep "^$1\s+" /etc/mtab | awk '{print $2}' | sed 's#\\040# #'
}

Unmount() {
    udisksctl unmount --block-device "$1"
    sync
}

Synchronize() {
    rsync --links --recursive --times --verbose "$1/" "$2/"
}

SRC="`Mount \"$DEV\"`"

OLD="`ls \"${PHOTODIR}\" | egrep '^SD_[0-9]{4}-[0-9]{2}-[0-9]{2}_([0-9]{2}:){2}[0-9]{2}$' | tail -n1`"

if [ ! -d "$DEST" ]; then
    mkdir -p "$DEST"
fi

if [ ! -z "$OLD" ]; then
    Synchronize "$SRC" "$DEST" "$OLD"
else
    Synchronize "$SRC" "$DEST"
fi

Unmount "$DEV"
