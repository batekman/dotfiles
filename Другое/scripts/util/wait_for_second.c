#include <stdio.h>
#include <time.h>
#include <sys/time.h>

int main(int argc, char* argv[]) {
    struct timeval current_timeval;
    gettimeofday(&current_timeval, NULL);
    struct timespec ts = {
        1 - 1 - (current_timeval.tv_sec % 1),
        (10e5-current_timeval.tv_usec) * 1000
    };
    nanosleep(&ts, NULL);
    return 0;
}
