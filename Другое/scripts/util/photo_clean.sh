#! /usr/bin/env bash

set -e

DEV="$1"

if [ -z "$DEV" ]; then
    exit 2
fi

Mount() {
    if ! egrep --quiet "^$1\s+" /etc/mtab ; then
        udisksctl mount --block-device "$1" > /dev/null
    fi
    egrep "^$1\s+" /etc/mtab | awk '{print $2}'
}

Unmount() {
    udisksctl unmount --block-device "$1"
    sync
}

SRC="`Mount \"$DEV\"`"

find "$SRC" -mindepth 3 -type f -iname "*.jpg" -exec rm -fv {} \;
find "$SRC" -mindepth 3 -type f -iname "*.avi" -exec rm -fv {} \;

Unmount "$DEV"
